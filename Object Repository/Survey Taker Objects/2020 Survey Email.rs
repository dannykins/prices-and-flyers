<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2020 Survey Email</name>
   <tag></tag>
   <elementGuidId>4a2987c9-d0ab-4c86-8efb-0daa3a8f0d4d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'email' and @placeholder = 'Email *' and @data-placeholder = 'Email *']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@type = 'email' and @placeholder = 'Email *' and @data-placeholder = 'Email *']</value>
   </webElementProperties>
</WebElementEntity>
