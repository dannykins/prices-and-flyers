<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2020 Survey Submit</name>
   <tag></tag>
   <elementGuidId>a96ee4f9-7ce2-4335-a42e-c01d7b1ebf04</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@type = 'submit' and @value = 'Record >>']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//input[@type = 'submit' and @value = 'Record >>']</value>
   </webElementProperties>
</WebElementEntity>
