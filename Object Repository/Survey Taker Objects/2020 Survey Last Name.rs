<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2020 Survey Last Name</name>
   <tag></tag>
   <elementGuidId>03ae6aaf-fec6-4a45-8bee-941071f8c191</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @class = 'text ' and @placeholder = 'Last Name' and @data-placeholder = 'Last Name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@type = 'text' and @class = 'text ' and @placeholder = 'Last Name' and @data-placeholder = 'Last Name']</value>
   </webElementProperties>
</WebElementEntity>
