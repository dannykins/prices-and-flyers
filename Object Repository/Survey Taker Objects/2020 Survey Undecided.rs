<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2020 Survey Undecided</name>
   <tag></tag>
   <elementGuidId>3ffc9484-000b-4fc2-9391-0d476114ef91</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//*[@type = 'radio' and @value = 'Undecided'])[${a}]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//*[@type = 'radio' and @value = 'Undecided'])[${a}]</value>
   </webElementProperties>
</WebElementEntity>
