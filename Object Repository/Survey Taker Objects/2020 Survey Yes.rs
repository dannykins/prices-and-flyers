<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2020 Survey Yes</name>
   <tag></tag>
   <elementGuidId>754d97bf-f49f-4e9e-9890-b5295e81c249</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//*[@type = 'radio' and @value = 'Yes'])[${a}]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//*[@type = 'radio' and @value = 'Yes'])[${a}]</value>
   </webElementProperties>
</WebElementEntity>
