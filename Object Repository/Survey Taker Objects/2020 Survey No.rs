<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2020 Survey No</name>
   <tag></tag>
   <elementGuidId>251646ec-c200-4548-bbdd-b1aeb20be3b0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>(//*[@type = 'radio' and @value = 'No'])[${a}]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>(//*[@type = 'radio' and @value = 'No'])[${a}]</value>
   </webElementProperties>
</WebElementEntity>
