<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2020 Survey First Name</name>
   <tag></tag>
   <elementGuidId>2d99e42a-ca52-4778-ad75-c9e5bea78360</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @class = 'text ' and @placeholder = 'First Name']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@type = 'text' and @class = 'text ' and @placeholder = 'First Name']</value>
   </webElementProperties>
</WebElementEntity>
