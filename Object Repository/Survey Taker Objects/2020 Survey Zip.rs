<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2020 Survey Zip</name>
   <tag></tag>
   <elementGuidId>0b329f1c-f806-4be8-b705-ff89b990c7a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@type = 'text' and @placeholder = 'Zip *']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[@type = 'text' and @placeholder = 'Zip *']</value>
   </webElementProperties>
</WebElementEntity>
