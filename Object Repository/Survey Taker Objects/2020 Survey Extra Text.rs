<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>2020 Survey Extra Text</name>
   <tag></tag>
   <elementGuidId>3bea1e5c-428f-4256-b175-d0bea784bf23</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//textarea[@name = 'fields[382]' and @id = 'ddform_382']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//textarea[@name = 'fields[382]' and @id = 'ddform_382']</value>
   </webElementProperties>
</WebElementEntity>
