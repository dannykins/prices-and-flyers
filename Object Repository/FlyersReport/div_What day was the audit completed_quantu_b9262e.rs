<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_What day was the audit completed_quantu_b9262e</name>
   <tag></tag>
   <elementGuidId>eba11de6-7f59-45de-92f0-112e483bae76</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='mG61Hd']/div/div[2]/div[2]/div[3]/div/div[2]/div/span/div/div/label/div/div/div[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>quantumWizTogglePaperradioOffRadio exportOuterCircle</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mG61Hd&quot;)/div[@class=&quot;freebirdFormviewerViewFormCard exportFormCard&quot;]/div[@class=&quot;freebirdFormviewerViewFormContent&quot;]/div[@class=&quot;freebirdFormviewerViewItemList&quot;]/div[@class=&quot;freebirdFormviewerViewNumberedItemContainer&quot;]/div[@class=&quot;freebirdFormviewerViewItemsItemItem&quot;]/div[2]/div[1]/span[@class=&quot;exportContent&quot;]/div[1]/div[@class=&quot;freebirdFormviewerViewItemsRadioOptionContainer&quot;]/label[@class=&quot;docssharedWizToggleLabeledContainer freebirdFormviewerViewItemsRadioChoice&quot;]/div[@class=&quot;docssharedWizToggleLabeledLabelWrapper exportLabelWrapper&quot;]/div[@class=&quot;quantumWizTogglePaperradioEl docssharedWizToggleLabeledControl freebirdThemedRadio freebirdThemedRadioDarkerDisabled freebirdFormviewerViewItemsRadioControl isActive isCheckedNext&quot;]/div[@class=&quot;quantumWizTogglePaperradioRadioContainer&quot;]/div[@class=&quot;quantumWizTogglePaperradioOffRadio exportOuterCircle&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='mG61Hd']/div/div[2]/div[2]/div[3]/div/div[2]/div/span/div/div/label/div/div/div[3]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='What day was the audit completed?'])[1]/following::div[10]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your answer'])[2]/following::div[21]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Monday'])[1]/preceding::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tuesday'])[1]/preceding::div[10]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//label/div/div/div[3]/div</value>
   </webElementXpaths>
</WebElementEntity>
