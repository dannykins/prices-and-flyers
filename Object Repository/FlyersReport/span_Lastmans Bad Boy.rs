<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Lastmans Bad Boy</name>
   <tag></tag>
   <elementGuidId>051f08d1-c346-49c7-a294-8c38ed0a1435</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='mG61Hd']/div[2]/div/div[2]/div[5]/div/div/div[2]/div/div[11]/label/div/div[2]/div/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>dir</name>
      <type>Main</type>
      <value>auto</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>docssharedWizToggleLabeledLabelText exportLabel freebirdFormviewerComponentsQuestionCheckboxLabel</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lastman's Bad Boy</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mG61Hd&quot;)/div[@class=&quot;freebirdFormviewerViewFormCard exportFormCard&quot;]/div[@class=&quot;freebirdFormviewerViewFormContent&quot;]/div[@class=&quot;freebirdFormviewerViewItemList&quot;]/div[@class=&quot;freebirdFormviewerViewNumberedItemContainer&quot;]/div[@class=&quot;m2&quot;]/div[@class=&quot;freebirdFormviewerComponentsQuestionBaseRoot&quot;]/div[@class=&quot;freebirdFormviewerComponentsQuestionCheckboxRoot&quot;]/div[1]/div[@class=&quot;freebirdFormviewerComponentsQuestionCheckboxChoice&quot;]/label[@class=&quot;docssharedWizToggleLabeledContainer freebirdFormviewerComponentsQuestionCheckboxCheckbox&quot;]/div[@class=&quot;docssharedWizToggleLabeledLabelWrapper exportLabelWrapper&quot;]/div[@class=&quot;docssharedWizToggleLabeledContent&quot;]/div[@class=&quot;docssharedWizToggleLabeledPrimaryText&quot;]/span[@class=&quot;docssharedWizToggleLabeledLabelText exportLabel freebirdFormviewerComponentsQuestionCheckboxLabel&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='mG61Hd']/div[2]/div/div[2]/div[5]/div/div/div[2]/div/div[11]/label/div/div[2]/div/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Home Depot'])[2]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Goemans Appliance'])[1]/following::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)=concat('Leon', &quot;'&quot;, 's')])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lowes'])[2]/preceding::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div[2]/div/div[11]/label/div/div[2]/div/span</value>
   </webElementXpaths>
</WebElementEntity>
