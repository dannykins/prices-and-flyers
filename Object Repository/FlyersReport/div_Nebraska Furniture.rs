<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Nebraska Furniture</name>
   <tag></tag>
   <elementGuidId>02d835e4-f1d9-46b1-90f4-e58537ed2df1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='mG61Hd']/div/div[2]/div[2]/div[4]/div/div[2]/div[20]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>freebirdFormviewerViewItemsCheckboxChoice</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Nebraska Furniture</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mG61Hd&quot;)/div[@class=&quot;freebirdFormviewerViewFormCard exportFormCard&quot;]/div[@class=&quot;freebirdFormviewerViewFormContent&quot;]/div[@class=&quot;freebirdFormviewerViewItemList&quot;]/div[@class=&quot;freebirdFormviewerViewNumberedItemContainer&quot;]/div[@class=&quot;freebirdFormviewerViewItemsItemItem&quot;]/div[2]/div[@class=&quot;freebirdFormviewerViewItemsCheckboxOptionContainer&quot;]/div[@class=&quot;freebirdFormviewerViewItemsCheckboxChoice&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='mG61Hd']/div/div[2]/div[2]/div[4]/div/div[2]/div[20]/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lowes'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kings'])[1]/following::div[14]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Orvilles'])[1]/preceding::div[18]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[20]/div</value>
   </webElementXpaths>
</WebElementEntity>
