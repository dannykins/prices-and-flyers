import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//import for robot command to use keyboard shortcuts
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

///////////////////////////////////////////////////////////
// Simple scraper to take out known Not Found Models     //
// Home Depot is a better example for the full  script   //
///////////////////////////////////////////////////////////

//Creating robot to use keyboard shortcuts
Robot r = new Robot()

//List of Not Founds and unique command to make sure there's no accidental repeats
List<String> RawList = ['FGGS3065PF','FFES3026TS','FGGF3059TD','FFGF3056TS','FFID2459VS']

List<String> NotFounds = RawList.unique(false)

String PricesPage = 'url'

//Open browser, go to prices page (reroutes to login page to begin)
WebUI.openBrowser('')
WebUI.maximizeWindow()
WebUI.navigateToUrl(PricesPage)


//Login
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginUsername'), 'N3fvSiAGolFODpSlvL1/gw==')
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginPassword'), 'PcMh+CIgUf5x7VH0M+V3xw==')
WebUI.click(findTestObject('PricesScraper/LoginButton'))

//go to prices page
WebUI.navigateToUrl(PricesPage)

//go to important skus
WebUI.click(findTestObject('PricesScraper/skuList1'))

WebUI.setText(findTestObject('PricesScraper/skuList2'), '$$Important')

r.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

//navigate to Lowes page
WebUI.click(findTestObject('PricesScraper/StoreSelect1'))

WebUI.delay(1)

r.keyPress(KeyEvent.VK_A)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_A)

r.keyPress(KeyEvent.VK_J)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_J)

r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)
WebUI.delay(2)
r.keyPress(KeyEvent.VK_TAB)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_TAB)
WebUI.delay(2)
r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)
WebUI.delay(1)

WebUI.click(findTestObject('PricesScraper/PopupCheckbox'))

WebUI.delay(1)

//For loop to cycle through the pages if there's over 1 page (100+ models) to scrape for
//20 is an arbitrary number that should cover any given instance (assuming the base scrapers aren't malfunctioning)
for (int b = 1; b <= 12; b++) {
	//For loop to cycle through model numbers, 1 page (up to 100 models at a time).
	for (int i = 1; i <= 100; i++) {
		WebUI.scrollToElement(findTestObject('PricesScraper/SKU', [('i') : i]), 1)
		Model = WebUI.getText(findTestObject('PricesScraper/SKU', [('i') : i]))
		WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))

		boolean unfound
		
		for (int c = 0; c <= NotFounds.size(); c++) {
			if (Model.equalsIgnoreCase(NotFounds[c])){
				
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '0')
				WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
				
				unfound = true
			}
		}
			
		println(unfound)
			
		if (b == 1 || b == 6 || b == 9 || b == 12) {
			if (unfound == false){
				NewPrice =  WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))
				WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), NewPrice)
				WebUI.click(findTestObject('PricesScraper/Submit_CopySurfaceToCart', [('i') : i]))
			}
		
		}
		
		WebUI.delay(1)
	}
	
	//Once the 100 models per page has been gone through, pause to let things settle and then reload the page for the next 100 models.
	WebUI.delay(1)

	WebUI.click(findTestObject('PricesScraper/LoadPageButton'))
}

//The multi-page loop has completed. Close the browser. Currently no-run for personal reasons, but would otherwise work just fine.
not_run: WebUI.closeBrowser('')

