import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.chrome.ChromeDriver as ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions as ChromeOptions
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import java.awt.Toolkit as Toolkit
import java.awt.datatransfer.Clipboard as Clipboard
import java.awt.datatransfer.StringSelection as StringSelection
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import java.time.DayOfWeek as DayOfWeek
import java.time.LocalDate as LocalDate
import java.time.format.TextStyle as TextStyle
import java.util.Locale as Locale
import java.util.List as List
import java.time.temporal.ChronoUnit as ChronoUnit
import java.time.MonthDay as MonthDay
import java.time.Month as Month

/////////////////////////////////////////////////////////////////////////////////
// Massive WIP that is only partially operational.                             //
//Key issues are the mass difference between sites and how they maintain flyers//
/////////////////////////////////////////////////////////////////////////////////

//Goal: Go to each website (currently only ABC Warehouse) and download the flyers to be uploaded to the maparmor site
//collecting the current date values
LocalDate date = LocalDate.now()

DayOfWeek day = date.getDayOfWeek()

//int month = date.getMonthValue()
//int dayno = date.getDayofMonth()
//List of the page numbers within the ABC Warehouse flyer page urls
List<String> abcnumber = ['_01_', '_02_', '_03_', '_04_', '_05_', '_06_', '_07_', '_08_', '_09_', '_10_', '_11_', '_12_']

//the holder for the page urls found during the for loop
List<String> files = new ArrayList()

//creates our robot friend
Robot r = new Robot()

//creates clipboard for the for loop
Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard()

WebUI.openBrowser('')

//ABC WAREHOUSE
not_run: WebUI.navigateToUrl('https://abcwarehouse.shoplocal.com/v3/index.html#!/abcwarehouse/store/2593041')

// checks for number of dynamic amount of pages on the website.
not_run: WebDriver driver = DriverFactory.getWebDriver()

not_run: int pagecount = driver.findElements(By.xpath('//*[contains(@back-img,\'akimages.shoplocal.com\') and contains(@back-img,\'.jpg\') and contains(@class,\'pageContainer\')]')).size()

//for loop to find the urls to download the flyer pages
not_run: for (int i = 0; i < pagecount; i++) {
    //Verify that the element exists and print "true" for "yes it does", "false" if it does not.
    //println(WebUI.verifyElementPresent(findTestObject('Flyers American Sites/find ABC flyer pages', [('abcnumber') : abcnumber.get(i)]), 2))
    //Take the attribute "back-img" from the object and name it "abcurl"
    abcurl = WebUI.getAttribute(findTestObject('Object Repository/Flyers American Sites/find ABC flyer pages'), 'back-img')

    //adds the current abcurl value into the list named "files"
    files.add(abcurl)

    //print the current value for the current file in the list
    //println(files[i])
    String company = 'ABC'

    String downloadpath = 'C:\\Users\\storm\\Downloads\\katalon\\'

    String text = ((downloadpath + company) + i) + '.jpg'

    StringSelection stringSelection = new StringSelection(text)

    clipboard.setContents(stringSelection, stringSelection)

    //verify if there's already an old file with the name in the folder
    WebUI.navigateToUrl('file:///C:/Users/storm/Downloads/katalon/')

    exists = WebUI.verifyTextPresent((company + i) + '.jpg', false, FailureHandling.OPTIONAL)

    WebUI.navigateToUrl(files[i])

    r.keyPress(KeyEvent.VK_CONTROL)

    r.keyPress(KeyEvent.VK_S)

    WebUI.delay(1)

    r.keyRelease(KeyEvent.VK_CONTROL)

    r.keyRelease(KeyEvent.VK_S)

    WebUI.delay(1)

    r.keyPress(KeyEvent.VK_CONTROL)

    r.keyPress(KeyEvent.VK_V)

    WebUI.delay(1)

    r.keyRelease(KeyEvent.VK_CONTROL)

    r.keyRelease(KeyEvent.VK_V)

    WebUI.delay(1)

    r.keyPress(KeyEvent.VK_ENTER)

    r.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(1)

    //if there's an old file, replace it.
    if (exists == true) {
        r.keyPress(KeyEvent.VK_TAB)

        r.keyRelease(KeyEvent.VK_TAB)

        WebUI.delay(1)

        r.keyPress(KeyEvent.VK_ENTER)

        r.keyRelease(KeyEvent.VK_ENTER)
    }
    
    WebUI.navigateToUrl('https://abcwarehouse.shoplocal.com/v3/index.html#!/abcwarehouse/store/2593041')
}

//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//ABT
//LocalDate abtdate = LocalDate.now()
not_run: LocalDate abtdate = LocalDate.of(2019, 10, 11)

not_run: abtcurrentmonth = abtdate.getMonthValue()

//MonthDay m = MonthDay.of(10,11)
//abtcurrentday = m.getDayofMonth()
not_run: String finaldate = (abtcurrentmonth + '/') + '10'

not_run: println(abtdate)

not_run: println(finaldate)

not_run: LocalDate mondayrange = abtdate.minus(2, ChronoUnit.DAYS)

not_run: LocalDate fridayrange = abtdate.minus(3, ChronoUnit.DAYS)

not_run: println(mondayrange)

not_run: println(fridayrange)

not_run: String monrange = '10/09'

//the holder for the page urls found during the for loop
not_run: List<String> abtfiles = new ArrayList()

not_run: WebUI.navigateToUrl('https://www.abt.com/news/store-ads')

//thought: grab current date in daysofyear. find way to compare that with text dates to grab the right urls. for loop to go through amount of urls.
//abtexists = WebUI.verifyTextPresent('10/22', false, FailureHandling.OPTIONAL)
//how many important elements are on the page
//int abtcount = abtdriver.findElements(By.xpath('//h4[contains(text(),\'10/10\')]/..')).size()
//make a list
//WebDriver abtdriver = DriverFactory.getWebDriver()
//List<WebElement> abtlist = abtdriver.findElements(By.xpath('//h4[contains(text(),\'10/10\')]/..'))
//println(abtcount)
//println(abtlist)
//thoughts: eventually use for loop with "i" to go through the elements on the page and snag the urls, downloading from there.
not_run: for (int i = 0; i < 1; i++) {
    abturl = WebUI.getAttribute(findTestObject('Object Repository/Flyers American Sites/imgnew'), 'href')

    println(abturl)

    abtfiles.add(abturl)

    String abtcompany = 'ABT'

    String abtdownloadpath = 'C:\\Users\\storm\\Downloads\\katalon\\'

    String abttext = ((abtdownloadpath + abtcompany) + i) + '.pdf'

    StringSelection abtstringSelection = new StringSelection(abttext)

    clipboard.setContents(abtstringSelection, abtstringSelection)

    //verify if there's already an old file with the name in the folder
    WebUI.navigateToUrl('file:///C:/Users/storm/Downloads/katalon/')

    exists = WebUI.verifyTextPresent((abtcompany + i) + '.pdf', false, FailureHandling.OPTIONAL)

    WebUI.navigateToUrl(abtfiles[i])

    r.keyPress(KeyEvent.VK_CONTROL)

    r.keyPress(KeyEvent.VK_S)

    WebUI.delay(1)

    r.keyRelease(KeyEvent.VK_CONTROL)

    r.keyRelease(KeyEvent.VK_S)

    WebUI.delay(1)

    r.keyPress(KeyEvent.VK_CONTROL)

    r.keyPress(KeyEvent.VK_V)

    WebUI.delay(1)

    r.keyRelease(KeyEvent.VK_CONTROL)

    r.keyRelease(KeyEvent.VK_V)

    WebUI.delay(1)

    r.keyPress(KeyEvent.VK_ENTER)

    r.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(1)

    //if there's an old file, replace it.
    if (exists == true) {
        r.keyPress(KeyEvent.VK_TAB)

        r.keyRelease(KeyEvent.VK_TAB)

        WebUI.delay(1)

        r.keyPress(KeyEvent.VK_ENTER)

        r.keyRelease(KeyEvent.VK_ENTER)
    }
    
    WebUI.navigateToUrl('https://www.abt.com/news/store-ads')
}

//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//Canada Home Depot
//List<String> hdnom = ['1.jpeg', '2.jpeg', '3.jpeg', '4.jpeg', '5.jpeg', '6.jpeg', '7.jpeg', '8.jpeg', '9.jpeg', '10.jpeg', '11.jpeg', '12.jpeg', '13.jpeg', '14.jpeg']
WebUI.navigateToUrl('https://flyers.smartcanucks.ca/home-depot-canada')

WebUI.click(findTestObject('Flyers - Canada Flyers/CanadaHD'))

canadaurl = WebUI.getUrl()

canadaallurl = (canadaurl + '/all')

WebUI.navigateToUrl(canadaallurl)

WebDriver hddriver = DriverFactory.getWebDriver()

int hdpagecount = hddriver.findElements(By.xpath('//*[contains(@src,\'.jpeg\') and contains(@src,\'flyers.smartcanucks.ca/uploads/pages/\')]')).size()

println(hdpagecount)

int hdpagecountb = hdpagecount + 1

//hdpageurlbase = /*[contains(@src,\'.jpeg\') and contains(@src,\'flyers.smartcanucks.ca/uploads/pages/\')]/src
hdpageurlbase = 'https://flyers.smartcanucks.ca/uploads/pages/138000/home-depot-on-flyer-november-7-to-131.jpeg'

println(hdpageurlbase)

String[] hdstring = hdpageurlbase.split('pages/')

String[] hdstring2 = hdstring[1].split('/home')

println(hdstring2[0])

hdflyernumber = (hdstring2[0])

for (int i = 1; i < hdpagecountb; i++) {
    String downloadurl = ((('https://flyers.smartcanucks.ca/uploads/pages/' + hdflyernumber) + '/home-depot-on-flyer-november-7-to-13') + 
    i) + '.jpeg'

    String company = 'CA HD'

    String downloadpath = 'C:\\Users\\storm\\Downloads\\katalon\\'

    String text = ((downloadpath + company) + i) + '.jpg'

    StringSelection stringSelection = new StringSelection(text)

    clipboard.setContents(stringSelection, stringSelection)

    //verify if there's already an old file with the name in the folder
    WebUI.navigateToUrl('file:///C:/Users/storm/Downloads/katalon/')

    exists = WebUI.verifyTextPresent((company + i) + '.jpg', false, FailureHandling.OPTIONAL)

    WebUI.navigateToUrl(downloadurl)

    r.keyPress(KeyEvent.VK_CONTROL)

    r.keyPress(KeyEvent.VK_S)

    WebUI.delay(1)

    r.keyRelease(KeyEvent.VK_CONTROL)

    r.keyRelease(KeyEvent.VK_S)

    WebUI.delay(1)

    r.keyPress(KeyEvent.VK_CONTROL)

    r.keyPress(KeyEvent.VK_V)

    WebUI.delay(1)

    r.keyRelease(KeyEvent.VK_CONTROL)

    r.keyRelease(KeyEvent.VK_V)

    WebUI.delay(1)

    r.keyPress(KeyEvent.VK_ENTER)

    r.keyRelease(KeyEvent.VK_ENTER)

    WebUI.delay(1)

    //if there's an old file, replace it.
    if (exists == true) {
        r.keyPress(KeyEvent.VK_TAB)

        r.keyRelease(KeyEvent.VK_TAB)

        WebUI.delay(1)

        r.keyPress(KeyEvent.VK_ENTER)

        r.keyRelease(KeyEvent.VK_ENTER)
    }
}

WebUI.delay(1)

WebUI.closeBrowser()

