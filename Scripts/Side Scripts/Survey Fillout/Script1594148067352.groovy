import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//Home Many Times Should This Run?
int times = 50

//Lists of plugin fake data
String[] firstNames = ['Dan', 'Steve','Sam','Patrick']
String[] lastNames = ['Stewart','O\'Loughlin','Ulmer','Krol']
String[] emails = ['playoffs88','howitzercat','zoomerjun','timeaus']
String[] message = ['This is message 1.','This is message 2.','This is message 3.']

WebUI.openBrowser('survey url here')

//loop to fill out survey a set number of times
for (int b = 1; b <= times; b++) {

	WebUI.delay(1)

	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey No', [('a') : 1]))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey No', [('a') : 2]))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey No', [('a') : 3]))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey Draining'))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey No', [('a') : 4]))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey No', [('a') : 5]))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey Undecided', [('a') : 6]))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey Yes', [('a') : 7]))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey Other', [('a') : 7]))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey No', [('a') : 8]))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey No', [('a') : 9]))
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey No', [('a') : 10]))

	//Random number generators to pick set of names/emails/etc.
	int firstNum = new Random().nextInt(firstNames.length - 1) + 1
	int lastNum = new Random().nextInt(lastNames.length - 1) + 1
	int emailNum = new Random().nextInt(emails.length - 1) + 1
	int messageNum = new Random().nextInt(message.length - 1) + 1
	int zip = new Random().nextInt(99999 - 10000) + 1
	String zipCode = zip.toString()
	
	//Fill out randomized personal information
	WebUI.setText(findTestObject('Object Repository/Survey Taker Objects/2020 Survey Extra Text'), message[messageNum])
	WebUI.setText(findTestObject('Object Repository/Survey Taker Objects/2020 Survey First Name'), firstNames[firstNum])
	WebUI.setText(findTestObject('Object Repository/Survey Taker Objects/2020 Survey Last Name'), lastNames[lastNum])
	WebUI.setText(findTestObject('Object Repository/Survey Taker Objects/2020 Survey Email'), (emails[emailNum]) + '@gmail.com')
	WebUI.setText(findTestObject('Object Repository/Survey Taker Objects/2020 Survey Zip'), zipCode)

	//Submit survey
	WebUI.click(findTestObject('Object Repository/Survey Taker Objects/2020 Survey Submit'))
	println(b)
	
	//wait 2 seconds then return to the survey to begin again/refresh to remove any cached information
	WebUI.delay(2)
	WebUI.back()
	WebUI.delay(1)
	WebUI.refresh()
}

WebUI.closeBrowser()

