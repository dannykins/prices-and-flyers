import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//import for robot command to use keyboard shortcuts
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

///////////////////////////////////////////////////////////
// Simple scraper to take out known Not Found Models     //
// Home Depot is a better example for the full  script   //
///////////////////////////////////////////////////////////

//Creating robot to use keyboard shortcuts
Robot r = new Robot()

//List of Not Founds and unique command to make sure there's no accidental repeats
List<String> RawList = ['FGHB2868TD', 'FG4H2272UF','FFHB2750TS','FGHD2368TF','FGHB2868TF','FGSS2635TF','FFSC2323TS','LFSC2324VF','FFSS2315TD','EFLS627UTT','FFSS2315TS',
	'FFSS2615TS', 'GCRE3060AF','FFBN1721TV','EFLS627UIW','EFME627UIW','FFSS2615TE','FFSS2615TP','EFME527UIW','EFLS527UIW','FFTR2021TS','FFEF3054TS','FFFU16F2VV',
	'FGBC5334VS','FFEC3625UB','FFEF3054TW','FFFC15M4TW','FFFU16F2VW','FFTR1814TW','FGCD2444SA','FFID2426TB','FFID2426TW','JTS5000SNSS','JTD5000SNSS','JTD3000SNSS',
	'PHP9036DJBB','GSS23GSKSS','RS25J500DSG','KCGS550ESS','RS25J500DSR','JGS760BELTS','PGP7030SLSS','LSCE305ST','RT21M6215SG','FCM22DLWW','NA36R5310FS','RT21M6215WW',
	'RT18M6215SG','RT18M6215WW','NX58H5600SS','FCM16SLWW','JBS60RKSS','GDF510PSMSS','FBD2400KS','FGHN2868TF','FFHB2750TP','EFME627UTT','EFDE317TIW','FFTR2021TW','NV51K6650DS',
	'WFW8620HC','WZF34X18DW','FGSS2335TF','FCRE305LAF','FFTR1821TW','FFTR1514TW','FFTR1814TB','PTD7000SNSS','PTS7000SNSS','KCGS356ESS','RT18M6215SR','NZ36R5330RK',
	'JGB700SEJSS','NZ30R5330RK','FGIH3047VF','FFFC25M4TW','FGID2479SF','FGIP2468UD','FFEF3054TB','FFTR1821TB','WEE750H0HV','WRF535SWHZ','WFW8620HW','JP5030SJSS','JGS760SELSS',
	'WFW6620HW','GTE18LSHSS','WDT730PAHV','JGB660SEJSS','NE59M4310SS','GDF630PSMSS','LFSS2312TE','LFSS2612TP','FGTR2037TF','EFME427UIW','FFFC20M4TW','FFGF3054TW','FFPS4533UU',
	'GFE28GSKSS','WOD51EC0HS','WOS72EC0HV','MSC21C6MFZ','RF260BEAESR','RF263BEAESR','GCI3061XB','WZC3122DW','WRT518SZFW','JGBS60REKSS','WZF34X16DW','WZC3115DW','FGHB2868TF',
	'FGHB2868TD','FGSS2635TF','GCRE3060AD','EFME527UIW','LFEF3054TD','FFFC20M4TW','LFTR1832TF','FCRG3052AS','FFHT1425VV','FFTR2021TB','FFEF3054TW','FFFC15M4TW','FFCD2418UW',
	'FFPS3133UM','PYE22KBLTS','NV51K6650DS','LMXS28626S','JTD3000SNSS','RF260BEAESR','LSXC22426S','RT21M6213SG','LSCE305ST','WM3700HVA','WM3700HVA','WRT519SZDM','WDT970SAHZ',
	'GTE18LSHSS','WRT519SZDW','RT18M6215WW','WF42H5000AW','NE59M4310SS','GDF510PSMSS','GDF530PGMWW','LFGH3054UF','FGID2479SF','FFHT1425VV','FFEF3054TB','FFFU16F2VW',
	'FFPS3133UM','PYE22KBLTS','WRF555SDFZ','JTD3000SNSS','JGS760SELSS','JGS760BELTS','LSXC22426S','PGP7030SLSS','NA30N6555TS','RT21M6213SG','WFW8620HW','RT21M6215SR',
	'WRT311FZDM','WFW6620HW','WM3700HVA','WM3900HWA','NE58K9430SS','WM3700HWA','GTE18LSHSS','WRT318FZDM','WRT519SZDW','NA36R5310FS','WDT730PAHV','RT18M6215SR','RT18M6215WW',
	'WF42H5000AW','NE59M4310SS','WA45N3050AW','GDF530PGMWW','FFWC38L2QS','FFTR1835VW','LFTR1835VF','JS760BLTS','GSS23GGKWW','WEG515S0FS','LDT5678ST','GDF630PMMES','SHE3AR75UC',
	'GDF510PGMWW','GTS18GTHWW','GTS21FSKSS', 'GYE22HBLTS','LFC21776ST','NV51R5511DG','NX58H5600SSAA','RS25J500DWW','WF45K6500AW','WFG540H0ES','WRS588FIHV','KICU500XBL',
	'NX58H9500WS','GTS16GSHSS','JB645RKSS','WFE320M0ES','WDF330PAHS','WDF520PADW','LGHX2636TF','FGIF3036TD','FGIF3036TF','FFEF3054TD','FFTR2021TW','LFTR1821TF','FFTR1821TW',
	'FFTR1425VW','GSS25GMHES','JGP5036SLSS','JGP5030SLSS','WRT318FZDW','WRT314TFDW']

List<String> NotFounds = RawList.unique(false)

String PricesPage = 'url'

//Open browser, go to prices page (reroutes to login page to begin)
WebUI.openBrowser('')
WebUI.maximizeWindow()
WebUI.navigateToUrl(PricesPage)


//Login
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginUsername'), 'N3fvSiAGolFODpSlvL1/gw==')
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginPassword'), 'PcMh+CIgUf5x7VH0M+V3xw==')
WebUI.click(findTestObject('PricesScraper/LoginButton'))

//go to prices page
WebUI.navigateToUrl(PricesPage)

//go to important skus
WebUI.click(findTestObject('PricesScraper/skuList1'))

WebUI.setText(findTestObject('PricesScraper/skuList2'), 'Jovan Lowes')

r.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

//navigate to Lowes page
WebUI.click(findTestObject('PricesScraper/StoreSelect1'))

WebUI.delay(1)

r.keyPress(KeyEvent.VK_L)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_L)

r.keyPress(KeyEvent.VK_O)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_O)

r.keyPress(KeyEvent.VK_W)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_W)

r.keyPress(KeyEvent.VK_E)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_E)

r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)
WebUI.delay(2)
r.keyPress(KeyEvent.VK_TAB)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_TAB)
WebUI.delay(2)
r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)
WebUI.delay(1)

WebUI.click(findTestObject('PricesScraper/PopupCheckbox'))

WebUI.delay(1)

//For loop to cycle through the pages if there's over 1 page (100+ models) to scrape for
//20 is an arbitrary number that should cover any given instance (assuming the base scrapers aren't malfunctioning)
for (int b = 1; b <= 12; b++) {
	//For loop to cycle through model numbers, 1 page (up to 100 models at a time).
	for (int i = 1; i <= 100; i++) {
		WebUI.scrollToElement(findTestObject('PricesScraper/SKU', [('i') : i]), 1)
		Model = WebUI.getText(findTestObject('PricesScraper/SKU', [('i') : i]))
		WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))

		boolean unfound
		
		for (int c = 0; c <= NotFounds.size(); c++) {
			if (Model.equalsIgnoreCase(NotFounds[c])){
				
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '0')
				WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
				
				unfound = true
			}
		}
			
		println(unfound)
			
		if (b == 3 || b == 6 || b == 9 || b == 12) {
			if (unfound == false){
				NewPrice =  WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))
				WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), NewPrice)
				WebUI.click(findTestObject('PricesScraper/Submit_CopySurfaceToCart', [('i') : i]))
			}
		
		}
		
		WebUI.delay(1)
	}
	
	//Once the 100 models per page has been gone through, pause to let things settle and then reload the page for the next 100 models.
	WebUI.delay(1)

	WebUI.click(findTestObject('PricesScraper/LoadPageButton'))
}

//The multi-page loop has completed. Close the browser. Currently no-run for personal reasons, but would otherwise work just fine.
not_run: WebUI.closeBrowser('')

