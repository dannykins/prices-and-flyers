import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.time.DayOfWeek as DayOfWeek
import java.time.LocalDate as LocalDate
import java.time.format.TextStyle as TextStyle
import java.util.Locale as Locale
import java.util.List as List

///////////////////////////////////////////////
// Simple script to fill out survey report   //
///////////////////////////////////////////////

//Getting the Day from the Date for Monday or Friday
LocalDate date = LocalDate.now()

DayOfWeek day = date.getDayOfWeek()

//Method 1 for snagging company names. Kept for posterity rather than cleanliness!
List<String> company = ['ABC Warehoue', 'Abt', 'Airport Appliance', 'Best Buy', 'Big Sandy Superstore', 'Brandsmart', 'Conns']

//Open browser, go to report page
WebUI.openBrowser('https://docs.google.com/forms/d/e/1FAIpQLSfmNYcWz5smniPOy9ZA0Wp5P_adrX8ibG25XDQnqPHVCMer9Q/viewform')

//Set name
WebUI.setText(findTestObject('PricesReport/First Name'), 'Dan')

WebUI.setText(findTestObject('PricesReport/Last name'), 'Stewart')

//for loop to run through each checkbox for companies included in Method 1.
for (int i = 0; i < company.size(); i++) {
    println(company.get(i))

    WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : company.get(i)]))
}

//Method 2, longer but kept for posterity.
WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Famous Tate']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Gerhards']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Grand']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Hahn']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'HH Gregg']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Home Depot']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Howards']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'JC Penney']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Jeff Lynch']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Karls Online']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Kings']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Lowes']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Nebraska Furniture']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Orvilles']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Pacific Sales']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'PC Richard']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Pieratts']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Queen City']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'RC Willey']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Sears']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Spencers TV']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Standard TV']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'Warehouse Discount Center']))

WebUI.click(findTestObject('FlyersReport/CompanyCheckbox', [('company') : 'West Coast Appliance']))

//If/Else for selecting the day. If it's Friday, check off the Canadian companies!
if (day.getDisplayName(TextStyle.FULL, Locale.getDefault()) == 'Monday') {
    WebUI.click(findTestObject('FlyersReport/DaySelection', [('day') : 'Monday'])) //Method 3 for Canadian company checkbox iteration
} else {
    WebUI.click(findTestObject('FlyersReport/DaySelection', [('day') : 'Friday']))

    for (int b = 1; b < 21; b++) {
        WebUI.click(findTestObject('FlyersReport/CanadaCompanyCheckbox2', [('a') : b]))
    }
}

//hit the submit button and close the browser. Complete!
WebUI.click(findTestObject('FlyersReport/SubmitButton'))

WebUI.delay(1)

WebUI.closeBrowser()

