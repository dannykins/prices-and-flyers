import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//import for robot command to use keyboard shortcuts
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

//Delay timer script Begin
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

//Begin Timer
int StartTime = 4 //4 am

LocalDateTime now = LocalDateTime.now()
DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME
String nowString = formatter.format(now)
println(nowString)
String nowhour = nowString.substring(0, 2)
String nowminute = nowString.substring(3, 5)
println(nowminute)
int currentHour = nowhour as Integer //current hour
int currentMinute = nowminute as Integer //current minute 

int timer

//If/Else to ensure proper number of seconds
if (currentHour > StartTime) {
   timer = ((((StartTime - currentHour) + 24) * 60) * 60) - (currentMinute * 60)
}else {
   timer = (((StartTime - currentHour) * 60) * 60) - (currentMinute * 60)
}

WebUI.delay(timer)
//Delay time script End

//Creating robot to use keyboard shortcuts
Robot r = new Robot()

String PricesPage = 'url'

//Open browser, go to prices page (reroutes to login page to begin)
WebUI.openBrowser(PricesPage)
WebUI.maximizeWindow()

//Login
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginUsername'), 'N3fvSiAGolFODpSlvL1/gw==')
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginPassword'), 'PcMh+CIgUf5x7VH0M+V3xw==')
WebUI.click(findTestObject('PricesScraper/LoginButton'))

//go to prices page
WebUI.navigateToUrl(PricesPage)

//navigate to Sears page
WebUI.click(findTestObject('PricesScraper/StoreSelect1'))
WebUI.setText(findTestObject('PricesScraper/StoreSelect2'), 'Sears')

//keyboard shortcut robot to navigate to correct page
//delays to make sure the keyboard shortcuts register and don't overlap
r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyPress(KeyEvent.VK_TAB)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_TAB)

WebUI.delay(1)

r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

//Uncheck to prevent future pop-ups
WebUI.uncheck(findTestObject('PricesScraper/PopupCheckbox'))

//For loop to cycle through the pages if there's over 1 page (100+ models) to scrape for
//20 is an arbitrary number that should cover any given instance (assuming the base scrapers aren't malfunctioning)
for (int b = 1; b <=20; b++){

	//For loop to cycle through model numbers, 1 page (up to 100 models at a time).
	for (int i = 1; i <= 100; i++) {
		
		//collect the bits and combine for the full model page
		urlFront = 'http://www.sears.com/search='
		WebUI.scrollToElement(findTestObject('PricesScraper/SKU', [('i') : i]), 1)
		urlMid = WebUI.getText(findTestObject('PricesScraper/SKU', [('i') : i]))
		WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
		urlFull = (urlFront + urlMid)
	
		//Open a New Tab for the model page via keyboard shortcut robot
		r.keyPress(KeyEvent.VK_CONTROL)
		r.keyPress(KeyEvent.VK_T)
		WebUI.delay(1)
		r.keyRelease(KeyEvent.VK_CONTROL)
		r.keyRelease(KeyEvent.VK_T)

		//Go to the model page in the new tab
		WebUI.switchToWindowIndex(1)
		WebUI.navigateToUrl(urlFull)

		//Determine if the model exists
		ModelKeyword = ('Model # ' + urlMid)
		ModelKeyword2 = (' ' + urlMid + ' ')
		ModelKeyword3 = ('Model # ' + urlMid + "/")

		//if results in search page, click on model page
		if (WebUI.verifyTextPresent(ModelKeyword2, false, FailureHandling.OPTIONAL) && WebUI.verifyTextPresent('found for', false, FailureHandling.OPTIONAL)) {
			WebUI.click(findTestObject('PricesScraper/SearchPageSelect'))
			WebUI.delay(2)
		}

		//on model page, find price
		if (WebUI.verifyTextPresent(ModelKeyword, false, FailureHandling.OPTIONAL)) {
			ModelExists = 'true'
		
			//Check if it's a false positive (e.g. "NEWDF3/AA" versus the searched for "NEWDF3")
			//If so, make price as 10.
			if (WebUI.verifyTextPresent(ModelKeyword3, false, FailureHandling.OPTIONAL)) {
				NewPrice = 10
			}
			//Check if it's a false positive (finding the model number, but it's water filter replacements)
			//If so, make price as 10.
			else if (WebUI.verifyTextPresent('Replacement Water Filter', false, FailureHandling.OPTIONAL) || WebUI.verifyTextPresent('Trim Kit', false, FailureHandling.OPTIONAL) || WebUI.verifyTextPresent('Wallpaper', false, FailureHandling.OPTIONAL)) {
				NewPrice = 10
			}
			
			//Else, check for and grab any discounted prices
			else if (WebUI.verifyElementPresent(findTestObject('PricesScraper/SearsRedModelPrice'),2, FailureHandling.OPTIONAL)) {
				NewPrice =  WebUI.getText(findTestObject('PricesScraper/SearsRedModelPrice')).replaceAll("[^0-9.]","")
			}
		
			//Else, check for and grab any new prices
			else if (WebUI.verifyElementPresent(findTestObject('PricesScraper/SearsNewModelPrice'),2, FailureHandling.OPTIONAL)) {
				NewPrice =  WebUI.getText(findTestObject('PricesScraper/SearsNewModelPrice')).replaceAll("[^0-9.]","")
				}
			
			//Else, check for and grab any cart-only prices
			else if (WebUI.verifyElementPresent(findTestObject('PricesScraper/SearsOldModelPrice'),2, FailureHandling.OPTIONAL)) {
				NewPrice =  WebUI.getText(findTestObject('PricesScraper/SearsOldModelPrice')).replaceAll("[^0-9.]","")
			}
		
			//Else, model exists, grab the prior price
			else {
				WebUI.switchToWindowIndex(0)
				NewPrice =  WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))
			}
		
			//Close the model page tab and return to the prices page
			WebUI.closeWindowIndex(1)
			WebUI.delay(1)
			WebUI.switchToWindowIndex(0)
			WebUI.delay(1)

			//convert the string to an int to be compared later.
			float NuPrice = ((NewPrice) as float)
			println(NuPrice)
		
			//Use the grabbed model price to determine the correct submit button
			//Weed out anything below $49 (usually a sign of being a replacement part instead of the full appliance)
			if (NuPrice <= 49.99) {
				WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '0')
				WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
			}
			//Otherwise, plug in the scraped price and hit "Copy Price" submit button
			else {
				WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), NewPrice)
				WebUI.click(findTestObject('PricesScraper/Submit_CopySurfaceToCart', [('i') : i]))
			}
		}
	
		//If the model number is not found on the page, close the model page, return to prices page and hit the "Not Found" submit button
		else {
			ModelExists = 'false'

			WebUI.closeWindowIndex(1)
			WebUI.switchToWindowIndex(0)

			WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), ' ')
			WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
		}
		
		WebUI.delay(1)
   }


	//Once the 100 models per page has been gone through, pause to let things settle and then reload the page for the next 100 models.
	WebUI.delay(1)
	WebUI.click(findTestObject('PricesScraper/LoadPageButton'))
}

//The multi-page loop has completed. Close the browser. Currently no-run for personal reasons, but would otherwise work just fine.
not_run: WebUI.closeBrowser('')
