import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//import for robot command to use keyboard shortcuts
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

//Creating robot to use keyboard shortcuts
Robot r = new Robot()

//login and navigate to prices page
WebUI.openBrowser('url')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('PricesScraper/LoginUsername'), 'N3fvSiAGolFODpSlvL1/gw==')

WebUI.setEncryptedText(findTestObject('PricesScraper/LoginPassword'), 'PcMh+CIgUf5x7VH0M+V3xw==')

WebUI.click(findTestObject('PricesScraper/LoginButton'))

WebUI.navigateToUrl('url')

//use for loop to go through i for each table. grab url. go to url. take screenshot.
for (i =1; i <3; i++ ){
	
	String genlocation1 =  'C:\\Users\\storm\\Downloads\\'+'screenshots'+i+'a.jpg' 
	String genlocation2 =  'C:\\Users\\storm\\Downloads\\'+'screenshots'+i+'b.jpg'
	
	println(genlocation1)
	println(genlocation2)
	
	WebUI.click(findTestObject('Screenshots/ScreenshotUrl', [('i') : i]))
	
	WebUI.delay(3)
	
	WebUI.switchToWindowIndex(1)
	
	//take screenshot and save it to specific directory
	WebUI.takeScreenshot('genlocation1')
	
	//add to cart
	if (WebUI.verifyElementPresent(findTestObject('Screenshots/AddToCartOne'),0, FailureHandling.OPTIONAL)) {
	
		WebUI.click(findTestObject('Screenshots/AddToCartOne'))
	}
	
	else if (WebUI.verifyElementPresent(findTestObject('Screenshots/AddToCartBestBuy'),0, FailureHandling.OPTIONAL)) {
		
		WebUI.click(findTestObject('Screenshots/AddToCartBestBuy'))
	}
	
	WebUI.takeScreenshot('genlocation2')
	
}
