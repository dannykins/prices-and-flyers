import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//import for robot command to use keyboard shortcuts
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

//Creating robot to use keyboard shortcuts
Robot r = new Robot()

//login and navigate to prices page
WebUI.openBrowser('')

WebUI.maximizeWindow()

WebUI.navigateToUrl('url')

WebUI.setText(findTestObject('PricesScraper/LoginUsername'), 'N3fvSiAGolFODpSlvL1/gw==')

WebUI.setEncryptedText(findTestObject('PricesScraper/LoginPassword'), 'PcMh+CIgUf5x7VH0M+V3xw==')

WebUI.click(findTestObject('PricesScraper/LoginButton'))

WebUI.navigateToUrl('url')

//go to important skus
WebUI.click(findTestObject('PricesScraper/skuList1'))

WebUI.setText(findTestObject('PricesScraper/skuList2'), 'Important')

r.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

//navigate to Lowes page
WebUI.click(findTestObject('PricesScraper/StoreSelect1'))

WebUI.delay(1)

//WebUI.setText(findTestObject('PricesScraper/StoreSelect2'), 'Home Depot')

r.keyPress(KeyEvent.VK_S)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_S)

r.keyPress(KeyEvent.VK_E)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_E)

r.keyPress(KeyEvent.VK_A)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_A)

r.keyPress(KeyEvent.VK_R)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_R)

r.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

for (i=1; i <=10;i++){
	
r.keyPress(KeyEvent.VK_TAB)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_TAB)

WebUI.delay(2)

r.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

WebUI.click(findTestObject('PricesScraper/PopupCheckbox'))

WebUI.delay(1)

//Grabbing Model Page
for (int i = 1; i <= 100; i++) {
    urlFront = 'http://www.sears.com/search='
	
	WebUI.scrollToElement(findTestObject('PricesScraper/SKU', [('i') : i]), 1)
	
	urlMid = WebUI.getText(findTestObject('PricesScraper/SKU', [('i') : i]))
	
	WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
	
    urlFull = (urlFront + urlMid)
	
	//Sears Specific - Grab BrandName
	//Brand = WebUI.getText(findTestObject('PricesScraper/ModelNumber2 - Copy', [('i') : i]))

    //Opening New Tab for Model Page
    r.keyPress(KeyEvent.VK_CONTROL)

    r.keyPress(KeyEvent.VK_T)

    WebUI.delay(1)

    r.keyRelease(KeyEvent.VK_CONTROL)

    r.keyRelease(KeyEvent.VK_T)

    WebUI.delay(1)

    //Going to Model Page
    WebUI.switchToWindowIndex(1)

    WebUI.delay(1)

    WebUI.navigateToUrl(urlFull)

    WebUI.delay(1)

    //Does the Model Exist?
    ModelKeyword = ('Model # ' + urlMid)

    //needs fixing for search page
	ModelKeyword2 = (' ' + urlMid + ' ')

	//if results in search page, click on model page
	if (WebUI.verifyTextPresent(ModelKeyword2, false, FailureHandling.OPTIONAL) && WebUI.verifyTextPresent('found for', false, FailureHandling.OPTIONAL)) {
		
		WebUI.click(findTestObject('PricesScraper/SearsPageSelect'))
		
//		WebUI.delay(2)
	}

	//on model page, find price    
	if (WebUI.verifyTextPresent(ModelKeyword, false, FailureHandling.OPTIONAL)) {

//        ModelExists = 'true'
		
		if (WebUI.verifyElementPresent(findTestObject('PricesScraper/SearsRedModelPrice'),0, FailureHandling.OPTIONAL)) {
			
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/SearsRedModelPrice')).replaceAll("[^0-9.]","")
		}
		
		else if (WebUI.verifyElementPresent(findTestObject('PricesScraper/SearsNewModelPrice'),0, FailureHandling.OPTIONAL)) {
			
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/SearsNewModelPrice')).replaceAll("[^0-9.]","")
			
		}
		
		else if (WebUI.verifyElementPresent(findTestObject('PricesScraper/SearsOldModelPrice'),0, FailureHandling.OPTIONAL)) {
			
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/SearsOldModelPrice')).replaceAll("[^0-9.]","")
		}
		
		else {
			WebUI.switchToWindowIndex(0)
			
//			WebUI.delay(1)
			
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))
		}
		
		
//		println(NewPrice)
		
        //Going back to Sears Page
		WebUI.closeWindowIndex(1)
		
        WebUI.switchToWindowIndex(0)

 //       WebUI.delay(1)

		//convert the string to an int to be compared later.
		float NuPrice = NewPrice as float
		
		println(NuPrice)
		
        //Copy Last Price Over
		if (NuPrice <= 49) {
			WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
			
			WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '')
		} else {
			WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))

			WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), NewPrice) 
		
			WebUI.click(findTestObject('PricesScraper/Submit_CopySurfaceToCart', [('i') : i]))
		}
		
		//Mark As Not Found
		
	} else {

        ModelExists = 'false'

        //Going back to Sears Page
		WebUI.closeWindowIndex(1)
		
        WebUI.switchToWindowIndex(0)

 //       WebUI.delay(1)

        WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '0')
		
		WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
    }
    
//    WebUI.delay(2)
}

WebUI.scrollToElement(findTestObject('PricesScraper/StoreSelect1'))

WebUI.click(findTestObject('PricesScraper/StoreSelect1'))

}

not_run: WebUI.closeBrowser('')

