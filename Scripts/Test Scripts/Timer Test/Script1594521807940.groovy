import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


LocalDateTime now = LocalDateTime.now()
LocalDateTime later = LocalDateTime.of(2020, 07, 11, 4, 15, 0)
println(now)
println(later)
DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME
String nowString = formatter.format(now)
String laterString = formatter.format(later)
String nowhour = nowString.substring(0, 2)
String nowminute = nowString.substring(3,5)
println(nowhour)
println(nowminute)

int currentHour = nowhour as Integer
int currentMinute = nowminute as Integer
println(currentHour)
println(currentMinute)

int beginHour = 4
int testHourA = 1
int testHourB = 23
int timerA = (4 - 1)
int timeB = ((4 - 23) + 24)

int timer
 
if (currentHour > beginHour) {
	timer = (((beginHour - currentHour) + 24) *60)*60
	println(timer)
	
}else {
	timer = ((beginHour - currentHour) * 60) *60
	println(timer)
}

println(timer)

WebUI.delay(timer)

WebUI.openBrowser('http://google.com/')


//time to actually condense things of what I actually need
//----------------------------------------------------------
//Delay timer script Begin
//import java.time.LocalDateTime
//import java.time.format.DateTimeFormatter

//Begin Timer
//int StartTime = 4 //4 am
//
//LocalDateTime now = LocalDateTime.now()
//DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_TIME
//String nowString = formatter.format(now)
//String nowhour = nowString.substring(0, 2)
//int currentHour = nowhour as Integer //current hour
//
//int timer
//
////If/Else to ensure proper number of seconds
//if (currentHour > StartTime) {
//   timer = (((StartTime - currentHour) + 24) * 60) * 60
//}else {
//   timer = ((StartTime - currentHour) * 60) *60
//}
//
//WebUI.delay(timer)
//Delay time script End

