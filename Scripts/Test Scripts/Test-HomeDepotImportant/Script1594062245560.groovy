import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//import for robot command to use keyboard shortcuts
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

//Creating robot to use keyboard shortcuts
Robot r = new Robot()

//login and navigate to prices page
WebUI.openBrowser('url')

WebUI.maximizeWindow()

WebUI.setText(findTestObject('PricesScraper/LoginUsername'), 'N3fvSiAGolFODpSlvL1/gw==')

WebUI.setEncryptedText(findTestObject('PricesScraper/LoginPassword'), 'PcMh+CIgUf5x7VH0M+V3xw==')

WebUI.click(findTestObject('PricesScraper/LoginButton'))

WebUI.navigateToUrl('url')

//go to important skus
WebUI.click(findTestObject('PricesScraper/skuList1'))

WebUI.setText(findTestObject('PricesScraper/skuList2'), 'Important')

r.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

//navigate to Lowes page
WebUI.click(findTestObject('PricesScraper/StoreSelect1'))

WebUI.delay(1)

//WebUI.setText(findTestObject('PricesScraper/StoreSelect2'), 'Home Depot')

r.keyPress(KeyEvent.VK_H)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_H)

r.keyPress(KeyEvent.VK_O)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_O)

r.keyPress(KeyEvent.VK_M)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_M)

r.keyPress(KeyEvent.VK_E)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_E)

r.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(2)

r.keyPress(KeyEvent.VK_TAB)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_TAB)

WebUI.delay(2)

r.keyPress(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

WebUI.click(findTestObject('PricesScraper/PopupCheckbox'))

WebUI.delay(1)

//Grabbing Model Page
for (int i = 1; i <= 32; i++) {
    urlFront = 'http://www.homedepot.com/s/'
	
	WebUI.scrollToElement(findTestObject('PricesScraper/SKU', [('i') : i]), 1)
	
	urlMid = WebUI.getText(findTestObject('PricesScraper/SKU', [('i') : i]))
	
	urlBack = '?NCNI-5'

    urlFull = (urlFront + urlMid + urlBack)

    //Opening New Tab for Model Page
    r.keyPress(KeyEvent.VK_CONTROL)

    r.keyPress(KeyEvent.VK_T)

    WebUI.delay(1)

    r.keyRelease(KeyEvent.VK_CONTROL)

    r.keyRelease(KeyEvent.VK_T)

    WebUI.delay(1)

    //Going to Model Page
    WebUI.switchToWindowIndex(1)

    WebUI.delay(1)

    WebUI.navigateToUrl(urlFull)

    WebUI.delay(1)

    //Does the Model Exist?
    ModelKeyword = ('Model # ' + urlMid)

    ModelKeyword2 = ('Model#  ' + urlMid)

	//if results in search page, click on model page
	if (WebUI.verifyTextPresent(ModelKeyword2, false, FailureHandling.OPTIONAL) && WebUI.verifyTextPresent('results for', false, FailureHandling.OPTIONAL)) {
		
			WebUI.switchToWindowIndex(0)
			
			WebUI.delay(1)
			
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))
				
	}

	//on model page, find price
	if (WebUI.verifyTextPresent(ModelKeyword, false, FailureHandling.OPTIONAL)) {
	
        println('The model exists')

        ModelExists = 'true'
		
		if (WebUI.verifyElementPresent(findTestObject('PricesScraper/HD_NewModelPrice'), 2, FailureHandling.OPTIONAL)) {
			
			NewPrice =  WebUI.getAttribute(findTestObject('PricesScraper/HD_NewModelPrice'), 'content')
			
			println(NewPrice)
		}
		
		else if (WebUI.verifyElementPresent(findTestObject('PricesScraper/HD_OldModelPrice'), 2, FailureHandling.OPTIONAL)) {
			
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/HD_OldModelPrice')).replaceAll("[^0-9.]","")
			
			println(NewPrice)
			
		}
		
		else {
			WebUI.switchToWindowIndex(0)
			
			WebUI.delay(1)
			
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))
		}
		
		
        //Going back to Home Depot Page
		WebUI.closeWindowIndex(1)
		
        WebUI.switchToWindowIndex(0)
		
		WebUI.delay(1)

		//convert the string to an int to be compared later.
		float NuPrice = NewPrice as float
		
		println(NuPrice)
		
        //Copy Last Price Over
		if (NuPrice <= 49) {
			WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
			
			WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '')
		} else {
			WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))

			WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), NewPrice) 
		
			WebUI.click(findTestObject('PricesScraper/Submit_CopySurfaceToCart', [('i') : i]))
		}
		
		//Mark As Not Found
    } else {
        println('The model doesn\'t exist.')

        ModelExists = 'false'

        //Going back to Home Depot Page
		WebUI.closeWindowIndex(1)
		
		WebUI.delay(1)
		
        WebUI.switchToWindowIndex(0)

        WebUI.delay(1)

        WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '0')
		
		WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
    }
    
    WebUI.delay(1)
}

WebUI.delay(5)

not_run: WebUI.closeBrowser('')