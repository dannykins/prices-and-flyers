import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.Keys as Keys
import java.util.List as List

///////////////////////////////////////////////
// Simple script to fill out survey report   //
///////////////////////////////////////////////

//Open browser, go to report page.
WebUI.openBrowser('https://docs.google.com/forms/d/e/1FAIpQLSdCkxj0BqQ_TuPClviIELFnUBTPx9BaHUvQ8uHWnYZVg6ki8w/viewform')

//Set name
WebUI.setText(findTestObject('PricesReport/First Name'), 'Dan')
WebUI.setText(findTestObject('PricesReport/Last name'), 'Stewart')

//while loop to check everything but the "No" checkbox.
while (WebUI.verifyElementPresent(findTestObject('PricesReport/PriceAuditCheckboxes'), 1)) {
    WebUI.click(findTestObject('PricesReport/PriceAuditCheckboxes'))
}

//Click submit and close the browser
WebUI.click(findTestObject('PricesReport/SubmitButton'))
WebUI.delay(1)
WebUI.closeBrowser()