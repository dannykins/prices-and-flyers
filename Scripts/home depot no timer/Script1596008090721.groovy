import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//import for robot command to use keyboard shortcuts
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

//Creating robot to use keyboard shortcuts
Robot r = new Robot()

//Delay timer script Begin
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


String PricesPage = 'url'

//Open browser, go to prices page (reroutes to login page to begin)
WebUI.openBrowser(PricesPage)
WebUI.maximizeWindow()

//Login
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginUsername'), 'N3fvSiAGolFODpSlvL1/gw==')
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginPassword'), 'PcMh+CIgUf5x7VH0M+V3xw==')
WebUI.click(findTestObject('PricesScraper/LoginButton'))

//go to prices page
WebUI.navigateToUrl(PricesPage)

//navigate to Home Depot page
WebUI.click(findTestObject('PricesScraper/StoreSelect1'))

WebUI.setText(findTestObject('PricesScraper/StoreSelect2'), 'Home Depot')

//keyboard shortcut robot to navigate to correct page
//delays to make sure the keyboard shortcuts register and don't overlap
r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyPress(KeyEvent.VK_TAB)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_TAB)

WebUI.delay(1)

r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

//Uncheck to prevent future pop-ups
WebUI.uncheck(findTestObject('PricesScraper/PopupCheckbox'))

//For loop to cycle through the pages if there's over 1 page (100+ models) to scrape for
//20 is an arbitrary number that should cover any given instance (assuming the base scrapers aren't malfunctioning)
for (int b = 1; b <= 20; b++) {
    //For loop to cycle through model numbers, 1 page (up to 100 models at a time).
    for (int i = 1; i <= 100; i++) {
        //collect the bits and combine for the full model page
        urlFront = 'http://www.homedepot.com/s/'
        WebUI.scrollToElement(findTestObject('PricesScraper/SKU', [('i') : i]), 1)
        urlMid = WebUI.getText(findTestObject('PricesScraper/SKU', [('i') : i]))
        //urlBack = '?NCNI-5'
        urlFull = (urlFront + urlMid)
        WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))

        //Open a New Tab for the model page via keyboard shortcut robot
        r.keyPress(KeyEvent.VK_CONTROL)
        r.keyPress(KeyEvent.VK_T)
        WebUI.delay(1)
        r.keyRelease(KeyEvent.VK_CONTROL)
        r.keyRelease(KeyEvent.VK_T)

        //Go to the model page in the new tab
        WebUI.switchToWindowIndex(1)
        WebUI.navigateToUrl(urlFull)

        //Determine if the model exists
        ModelKeyword = ('Model # ' + urlMid)
        ModelKeyword2 = ('Model#  ' + urlMid)
        SearchPageKeyword = (('results for \'' + urlMid) + '\'')

        //If the model does exist (following two If Loops). If the model does not exist, skip to the Else loop.
        //If loop for cases when the model page reroutes to a search page (but the model does still exist!)
        //Grab last known price, then submit that!
        if (WebUI.verifyTextPresent(ModelKeyword2, false, FailureHandling.OPTIONAL) && WebUI.verifyTextPresent('results for', 
            false, FailureHandling.OPTIONAL)) {
            println('true')
            println('We\'ve hit a search page!')

            WebUI.closeWindowIndex(1)
            WebUI.delay(1)
            WebUI.switchToWindowIndex(0)
            WebUI.delay(1)

            WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
            NewPrice = WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))
            println(NewPrice)

            WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), NewPrice)
            WebUI.click(findTestObject('PricesScraper/Submit_CopySurfaceToCart', [('i') : i])) 
        }
			
		//If loop for arriving on model page, grab a price
        else if (WebUI.verifyTextPresent(ModelKeyword, false, FailureHandling.OPTIONAL)) {
            ModelExists = 'true'
            println('true')

			//If loop for if the model is discontinued, set price to 0
            if (WebUI.verifyElementPresent(findTestObject('PricesScraper/HD_discontinued'), 2, FailureHandling.OPTIONAL)) {
                Discontinued = 'true'
                NewPrice = 0
                println('Model is Discontinued')
            } 
			//ElseIf Fix to prevent a cart-only price getting through.
			else if (WebUI.verifyTextPresent('See Lower Price in Cart', false, FailureHandling.OPTIONAL)) {
                WebUI.switchToWindowIndex(0)
                WebUI.delay(1)
                WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
                NewPrice = WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))
                println('Model has no obvious Surface Price')
            }
			//ElseIf for if there's been a discount, grab the new price
			else if (WebUI.verifyElementPresent(findTestObject('PricesScraper/HD_NewModelPrice'), 2, FailureHandling.OPTIONAL)) {
                NewPrice = WebUI.getAttribute(findTestObject('PricesScraper/HD_NewModelPrice'), 'content')
                println('Model has been discounted. Grabbing new price!')
            }
			//ElseIf for if there is a discount but not new price to grab
			else if (WebUI.verifyElementPresent(findTestObject('PricesScraper/HD_OldModelPrice'), 2, FailureHandling.OPTIONAL)) {
                NewPrice = WebUI.getText(findTestObject('PricesScraper/HD_OldModelPrice')).replaceAll('[^0-9.]', '')
                println('Model has been discounted, but there is no new price')
            } 
			//Else all else fails, grab the last price and submit the model price
			else {
                WebUI.switchToWindowIndex(0)
                WebUI.delay(1)
                WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
                NewPrice = WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))
                println('No other price data exists. Using prior price.')
            }

			//Close the model page tab and return to the prices page
            WebUI.closeWindowIndex(1)
            WebUI.delay(1)
            WebUI.switchToWindowIndex(0)
            WebUI.delay(1)

			//convert the string to an int to be compared later.
            float NuPrice = ((NewPrice) as float)
            println(NuPrice)

			//Use the grabbed model price to determine the correct submit button
			//If price is 0 (discontinued), click the "Discontinued" submit button
            if (NuPrice == 0) {
                WebUI.click(findTestObject('PricesScraper/Submit_Discontinued', [('i') : i]))
            } 
			//Weed out anything below $49 (usually a sign of being a replacement part instead of the full appliance)
			else if (NuPrice <= 49.99) {
                WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
                WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '0')
                WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
            } 
			//Otherwise, plug in the scraped price and hit "Copy Price" submit button
			else {
                WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
                WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), NewPrice)
                WebUI.click(findTestObject('PricesScraper/Submit_CopySurfaceToCart', [('i') : i]))
            }
        } 
		//If the model number is not found on the page, close the model page, return to prices page and hit the "Not Found" submit button
		else {
            ModelExists = 'false'

            WebUI.closeWindowIndex(1)
            WebUI.delay(1)
            WebUI.switchToWindowIndex(0)
            WebUI.delay(1)
            WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '0')
            WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
        }
        
        WebUI.delay(1)
    }
    
    //Once the 100 models per page has been gone through, pause to let things settle and then reload the page for the next 100 models.
    WebUI.delay(1)

    WebUI.click(findTestObject('PricesScraper/LoadPageButton'))
}

//The multi-page loop has completed. Close the browser. Currently no-run for personal reasons, but would otherwise work just fine.
not_run: WebUI.closeBrowser('')

