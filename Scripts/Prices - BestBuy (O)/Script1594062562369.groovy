import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//import for robot command to use keyboard shortcuts
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

//Creating robot to use keyboard shortcuts
Robot r = new Robot()

//Open browser, go to prices page (reroutes to login page to begin)
WebUI.openBrowser('url')
WebUI.maximizeWindow()

//Login
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginUsername'), 'N3fvSiAGolFODpSlvL1/gw==')
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginPassword'), 'PcMh+CIgUf5x7VH0M+V3xw==')
WebUI.click(findTestObject('PricesScraper/LoginButton'))

//go to prices page
WebUI.openBrowser('url')

//navigate to Best Buy page
WebUI.click(findTestObject('PricesScraper/StoreSelect1'))
WebUI.setText(findTestObject('PricesScraper/StoreSelect2'), 'Best Buy')

//keyboard shortcut robot to navigate to correct page
//delays to make sure the keyboard shortcuts register and don't overlap
r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyPress(KeyEvent.VK_TAB)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_TAB)

WebUI.delay(1)

r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

//Uncheck to prevent future pop-ups
WebUI.uncheck(findTestObject('PricesScraper/PopupCheckbox'))

//For loop to cycle through the pages if there's over 1 page (100+ models) to scrape for
//20 is an arbitrary number that should cover any given instance (assuming the base scrapers aren't malfunctioning)
for (int b = 1; b <=20; b++){

	//For loop to cycle through model numbers, 1 page (up to 100 models at a time).
	for (int i = 1; i <= 100; i++) {
		urlFront = 'https://www.bestbuy.com/site/searchpage.jsp?id=pcat17071&st='
		WebUI.scrollToElement(findTestObject('PricesScraper/SKU', [('i') : i]), 1)
		urlMid = WebUI.getText(findTestObject('PricesScraper/SKU', [('i') : i]))
		urlFull = (urlFront + urlMid)

		//Open a New Tab for the model page via keyboard shortcut robot
		r.keyPress(KeyEvent.VK_CONTROL)
		r.keyPress(KeyEvent.VK_T)
		WebUI.delay(1)
		r.keyRelease(KeyEvent.VK_CONTROL)
		r.keyRelease(KeyEvent.VK_T)

		//Go to the model page in the new tab
		WebUI.switchToWindowIndex(1)
		WebUI.navigateToUrl(urlFull)

		//Determine if the model exists
		ModelKeyword = ('Model:' + urlMid)
		ModelKeyword2 = ('Model: ' + urlMid)
	
		if (WebUI.verifyTextPresent(ModelKeyword, false, FailureHandling.OPTIONAL) || WebUI.verifyTextPresent(ModelKeyword2, false, FailureHandling.OPTIONAL)) {
			ModelExists = 'true'
			WebUI.switchToWindowIndex(0)
			WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))		
		
			//Going back to Prices Page
			WebUI.closeWindowIndex(1)
			WebUI.switchToWindowIndex(0)

			//convert the string to an int to be compared later.
			float NuPrice = NewPrice as float
		
			//Use the grabbed model price to determine the correct submit button
			//If there is no prior price, but the model exists, type out a notice for manual input later
			if (NuPrice <= 1) {
				WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), 'No Prev Price')
			}
			//Weed out anything below $49 (usually a sign of being a replacement part instead of the full appliance)
			else if (NuPrice <= 49) {
				WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '0')
				WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
			} 
			//Otherwise, plug in the scraped price and hit "Copy Price" submit button
			else {
				WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), NewPrice) 
				WebUI.click(findTestObject('PricesScraper/Submit_CopySurfaceToCart', [('i') : i]))
			}
		}

		
		//If the model number is not found on the page, close the model page, return to prices page and hit the "Not Found" submit button
		else {
			ModelExists = 'false'

			WebUI.closeWindowIndex(1)
		    WebUI.switchToWindowIndex(0)

			WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '0')
			WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
		}
    
		//Random Number to Delay to look normal and "I'm not a bot :D"		
		int DelayTime = new Random().nextInt(12 - 2 + 1) + 2
		WebUI.delay(DelayTime)
	}
	
	//Once the 100 models per page has been gone through, pause to let things settle and then reload the page for the next 100 models.
	WebUI.delay(1)
	WebUI.click(findTestObject('PricesScraper/LoadPageButton'))
}

//The multi-page loop has completed. Close the browser. Currently no-run for personal reasons, but would otherwise work just fine.
not_run: WebUI.closeBrowser('')

