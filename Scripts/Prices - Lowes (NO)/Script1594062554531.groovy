import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//import for robot command to use keyboard shortcuts
import java.awt.Robot as Robot
import java.awt.event.KeyEvent as KeyEvent

/////////////////////////////////////////////////////////
// This one is current unusable, as the retailer site  //
// is pretty good and fast at catching automation!     //
/////////////////////////////////////////////////////////

//Creating robot to use keyboard shortcuts
Robot r = new Robot()

//Open browser, go to prices page (reroutes to login page to begin)
WebUI.openBrowser('url')
WebUI.maximizeWindow()

//Login
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginUsername'), 'N3fvSiAGolFODpSlvL1/gw==')
WebUI.setEncryptedText(findTestObject('PricesScraper/LoginPassword'), 'PcMh+CIgUf5x7VH0M+V3xw==')
WebUI.click(findTestObject('PricesScraper/LoginButton'))

//go to prices page
WebUI.openBrowser('url')

//navigate to Lowes page
WebUI.click(findTestObject('PricesScraper/StoreSelect1'))
WebUI.setText(findTestObject('PricesScraper/StoreSelect2'), 'Lowes')

//keyboard shortcut robot to navigate to correct page
//delays to make sure the keyboard shortcuts register and don't overlap
r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

r.keyPress(KeyEvent.VK_TAB)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_TAB)

WebUI.delay(1)

r.keyPress(KeyEvent.VK_ENTER)
WebUI.delay(1)
r.keyRelease(KeyEvent.VK_ENTER)

WebUI.delay(1)

//Uncheck to prevent future pop-ups
WebUI.uncheck(findTestObject('PricesScraper/PopupCheckbox'))

WebUI.delay(1)
//Grabbing Model Page
for (int i = 1; i <= 100; i++) {
    urlFront = 'http://www.lowes.com/search?searchTerm='
	
	WebUI.scrollToElement(findTestObject('PricesScraper/SKU', [('i') : i]), 1)
	
	urlMid = WebUI.getText(findTestObject('PricesScraper/SKU', [('i') : i]))

    urlFull = (urlFront + urlMid)
	
	println(urlFull)

    //Opening New Tab for Model Page
    r.keyPress(KeyEvent.VK_CONTROL)

    r.keyPress(KeyEvent.VK_T)

    WebUI.delay(1)

    r.keyRelease(KeyEvent.VK_CONTROL)

    r.keyRelease(KeyEvent.VK_T)

    WebUI.delay(1)

    //Going to Model Page
    WebUI.switchToWindowIndex(1)

    WebUI.delay(1)

    WebUI.navigateToUrl(urlFull)

    WebUI.delay(1)

    //Does the Model Exist?
    ModelKeyword = ('Model # ' + urlMid)

    ModelKeyword2 = ('Model: #' + urlMid)

    println(ModelKeyword)

    println(ModelKeyword2)

    if (WebUI.verifyTextPresent(ModelKeyword, false, FailureHandling.OPTIONAL) || WebUI.verifyTextPresent(ModelKeyword2, 
        false, FailureHandling.OPTIONAL)) {
        println('The model exists')

        ModelExists = 'true'
		
		if (WebUI.verifyElementPresent(findTestObject('PricesScraper/LowesBasePrice'),0, FailureHandling.OPTIONAL)) {
			
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/LowesBasePrice')).replaceAll("[^0-9.]","")
		}
		
		else if (WebUI.verifyElementPresent(findTestObject('PricesScraper/LowesCutPrice'),0, FailureHandling.OPTIONAL)) {
			
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/LowesCutPrice')).replaceAll("[^0-9.]","")
			
		}
	
		
		
		else {
			WebUI.switchToWindowIndex(0)
			
//			WebUI.delay(1)
			
			NewPrice =  WebUI.getText(findTestObject('PricesScraper/LastGrabbedPrice'))
		}
		
		WebUI.switchToWindowIndex(0)
		
		//convert the string to an int to be compared later.
		float NuPrice = NewPrice as float
		
		println(NuPrice)
		
        //Copy Last Price Over
		if (ModelExists == 'true' && NuPrice <= 1) {
			WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
			
			WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), 'Exists')
		}
		
		else if (NuPrice <= 49) {
			WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))
			
			WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '')
		} else {
			WebUI.click(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]))

			WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), NewPrice) 
		
			WebUI.click(findTestObject('PricesScraper/Submit_CopySurfaceToCart', [('i') : i]))
		}
    }

		
		//Mark As Not Found
		
		else {
			
			//Going back to Prices Page
					
			WebUI.switchToWindowIndex(1)
			
			if (WebUI.verifyTextPresent('Access Denied', false, FailureHandling.OPTIONAL)) {
		
				ModelExists = 'denied'
				
				WebUI.switchToWindowIndex(0)
		
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), 'Access Denied')
				
				WebUI.click(findTestObject('PricesScraper/Submit_SiteError', [('i') : i]))
			}
			
			else {
				WebUI.switchToWindowIndex(0)
				
				ModelExists = 'false'
				
				WebUI.setText(findTestObject('PricesScraper/SurfacePriceInput', [('i') : i]), '0')
					
				WebUI.click(findTestObject('PricesScraper/Submit_NotFound', [('i') : i]))
			}
    }
    
		WebUI.closeWindowIndex(1)
		
		WebUI.switchToWindowIndex(0)
		
		//Random Number to Delay to look normal and "I'm not a bot :D"
		int DelayTime = new Random().nextInt(60 - 2 + 1) + 5
		
		println(DelayTime)
		
		WebUI.delay(DelayTime)
		

}

not_run: WebUI.closeBrowser('')
