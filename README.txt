# Readme

This is a repository for work-related scripts. This includes one-time-use scripts as well as daily-run scripts, as well as a folder of test scripts.

Main goals are for the automation of daily and/or weekly tasks, as well as continued personal practice within the Kalanton environs. 

Initial project began September 2019, first commit to bitbucket December 2019.

Concise Commit History
Date 		Commit #	Description
2019‑12‑13	c76d373		Initial Commit, upload of initial price scraping script (Home Depot), flyer wip script
2019‑12‑19	23c134d		Updates price scraping scripts to include Lowes and Sears, fixes to price scraper objects
2019‑12‑23	53903a0		Updates to Lowes, Sears for increased functionality
2020‑01‑10	291711d		Remove false negatives for Lowes script
2020‑01‑16	7fb8a68		Updated functionality for Home Depot script, addition of Best Buy script (includes randomly generated delay between scrapes for bot detection purposes)
2020‑05‑12	3b716e0		Overhaul for Home Depot script (discontinued/not found/etc models) due to website update, addition of AJ Madison script
2020‑06‑25	56ab8b4		Added multi-page functionality for scraper scripts, attempt to evade automation detection by Lowes, fixed daily survey object errors
2020‑06‑29	28de6e4		Code cleanup 1.0, formatting, adding more informative comments, etc
2020-07-01	f965fcc		Code cleanup 1.2, updating scripts due to website updates
2020-07-04	df4161a		Fixes to weekly surveys due to website updates
2020-07-06	4166148		Further code cleanup, scrubbing of sensitive information and added text encryption (login info/internal urls)
2020-07-07	2002976		Added website survey script, organized scripts to contain test- and side-scripts
2020-07-10	3f31853		Added WalMart scraper, ReadMe file
2020-08-05				Edited timer for Home Depot and Sears


